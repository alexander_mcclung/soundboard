package macro
{
    import flash.utils.ByteArray;
    /// Simple macro class that executes a series of steps in sequence.
    public class Macro
    {
        /// List of steps to execute, one at a time.
        /// Each stip must support a complete callback that it will call when that step is complete.
        /// When that is called, this class will be listening and move on to the next step.
        private var mSteps:/*MacroStep*/Array;

        private var mIndex:Number = 0;

        /// Create the macro
        public function Macro()
        {
            mSteps = new Array();
        }

        //// Add a new step to the macro.
        public function AddStep(step:MacroStep):void
        {
            mSteps.push( step );
        }

        /// Begin executing the steps in order
        public function Execute():void
        {
            mIndex = 0;
            _ExecuteNext();
        }

        /// Execute the next step in the macro
        private function _ExecuteNext():void
        {
            if ( mIndex < mSteps.length )
            {
                mSteps[ mIndex++ ].Execute( _ExecuteNext );
            }
            // TODO: Maybe dispatch an event or something when the macro has finished
        }
    }
}