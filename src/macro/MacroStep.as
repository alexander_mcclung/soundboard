package macro
{
    /// Interface for a single step in the macro
    public interface MacroStep
    {
        /// Execute the step and call the supplied complete callback when finished.
        /// The macro will not call the next step until this callback is invoked.
        function Execute(completeCallback:Function):void;
    }
}