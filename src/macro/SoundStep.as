package macro
{
    import flash.events.Event;
    import sound.SoundEffect;

    public class SoundStep implements MacroStep
    {
        private var mSound:SoundEffect;
        private var mCompleteCallback:Function;
        private var mWaitForComplete:Boolean;
        private var mPlaybackRate:Number;

        public function SoundStep(sound:SoundEffect, waitForComplete:Boolean, playbackRate:Number=1.0)
        {
            mSound = sound;
            mSound.addEventListener( Event.SOUND_COMPLETE, _OnSoundComplete );

            mWaitForComplete = waitForComplete;
            mPlaybackRate = playbackRate;
        }

        public function Execute(completeCallback:Function):void
        {
            trace( "macro.SoundStep :: Execute :: mSound.location = " + mSound.Location + " rate " + mPlaybackRate );
            mCompleteCallback = completeCallback;

            mSound.Rate = mPlaybackRate;
            mSound.Play();

            if( !mWaitForComplete )
            {
                var temp:Function = mCompleteCallback;
                mCompleteCallback = undefined;
                temp();
            }
        }

        private function _OnSoundComplete(e:Event):void
        {
            if( mWaitForComplete && null != mCompleteCallback )
            {
                var temp:Function = mCompleteCallback;
                mCompleteCallback = undefined;
                temp();
            }
        }
    }
}