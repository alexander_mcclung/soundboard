package macro
{
    import flash.events.TimerEvent;
    import flash.utils.Timer;

    public class WaitStep implements MacroStep
    {
        private var mTimer:Timer;
        private var mCompleteCallback:Function;

        public function WaitStep(delayTimeMs:int)
        {
            mTimer = new Timer( delayTimeMs, 1 );
            mTimer.addEventListener( TimerEvent.TIMER_COMPLETE, _OnWaitComplete );
        }

        public function Execute(complete:Function):void
        {
            mCompleteCallback = complete;
            mTimer.start();
        }

        private function _OnWaitComplete(e:TimerEvent):void
        {
            var temp:Function = mCompleteCallback;
            mCompleteCallback = undefined;
            temp();
        }
    }
}