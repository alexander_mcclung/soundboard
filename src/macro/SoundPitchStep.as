package macro
{
    import sound.SoundEffect;

    /**
     * ...
     * @author Alex McClung
     */
    public class SoundPitchStep implements MacroStep
    {
        private var mSound:SoundEffect;
        private var mPlaybackRate:Number;

        public function SoundPitchStep(sound:SoundEffect, playbackRate:Number=1.0)
        {
            mSound = sound;
            mPlaybackRate = playbackRate;
        }

        public function Execute(completeCallback:Function):void
        {
            //trace( "macro.SoundPitchStep :: Execute :: mSound.location = " + mSound.Location + " mSound.playing = " + mSound.Playing + " mPlaybackRate = " + mPlaybackRate );
            mSound.Rate = mPlaybackRate;
            completeCallback();
        }
    }

}