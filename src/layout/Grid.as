package layout
{
    /**
     * Grid layout class
     * @author amcclung
     */
    public class Grid
    {
        private var mCols:int = 1;
        private var mRows:int = 1;
        private var mPadding:int = 0;

        private var mOriginX:int = 0;
        private var mOriginY:int = 0;
        private var mWidth:int  = 100;
        private var mHeight:int = 100;

        private var mButtons:Array;

        public function Grid(numCols:int, numRows:int)
        {
            mCols = numCols;
            mRows = numRows;

            mButtons = new Array();
        }

        public function SetPadding(padding:int):void
        {
            mPadding = padding;
        }
        
        public function SetOrigin(x:int, y:int):void
        {
            mOriginX = x;
            mOriginY = y;
        }

        public function SetSize(width:int, height:int):void
        {
            mWidth = width;
            mHeight = height;
        }

        public function AddButton(button:*):void
        {
            mButtons.push( button );
        }

        public function Clear():void
        {
            mButtons = new Array();
        }

        public function Draw():void
        {
            var posX:Number = mOriginX;
            var posY:Number = mOriginY;
            var btnWidth:Number  = (mWidth - ((mCols-1) * mPadding)) / mCols;
            var btnHeight:Number = (mHeight - ((mRows - 1) * mPadding)) / mRows;

            var btn:*;
            var col:int = 0;
            for (var i:int = 0; i < mButtons.length; i++)
            {
                btn = mButtons[i];

                btn.SetSize( btnWidth, btnHeight );
                btn.x = posX;
                btn.y = posY;

                posX += btnWidth + mPadding;
                ++col;
                if ( col >= mCols )
                {
                    col = 0;

                    posX = mOriginX;
                    posY += btnHeight + mPadding;
                }
            }
        }
    }

}