package struct
{
    import flash.ui.Keyboard;
    /**
     * Defines all the hotkeys that can be used in the buttons.
     * Because I don't want to clutter the main class up with a bunch of junk
     * @author amcclung
     */
    public class HotKeys
    {
        public static const HOTKEYS:Array = [
            Keyboard.NUMBER_1,
            Keyboard.NUMBER_2,
            Keyboard.NUMBER_3,
            Keyboard.NUMBER_4,
            Keyboard.NUMBER_5,
            Keyboard.NUMBER_6,
            Keyboard.NUMBER_7,
            Keyboard.NUMBER_8,
            Keyboard.NUMBER_9,
            Keyboard.NUMBER_0,

            Keyboard.Q,
            Keyboard.W,
            Keyboard.E,
            Keyboard.R,
            Keyboard.T,
            Keyboard.Y,
            Keyboard.U,
            Keyboard.I,
            Keyboard.O,
            Keyboard.P,
            Keyboard.LEFTBRACKET,
            Keyboard.RIGHTBRACKET,
            Keyboard.BACKSLASH,

            Keyboard.A,
            Keyboard.S,
            Keyboard.D,
            Keyboard.F,
            Keyboard.G,
            Keyboard.H,
            Keyboard.J,
            Keyboard.K,
            Keyboard.L,
            Keyboard.SEMICOLON,
            Keyboard.QUOTE,

            Keyboard.Z,
            Keyboard.X,
            Keyboard.C,
            Keyboard.V,
            Keyboard.B,
            Keyboard.N,
            Keyboard.M,
            Keyboard.COMMA,
            Keyboard.PERIOD,
            Keyboard.SLASH
        ];

        private var mCurrentIndex:Number = 0;

        public function HotKeys()
        {
            Reset();
        }

        public function Reset():void
        {
            mCurrentIndex = 0;
        }

        public function Next():int
        {
            var next:int = -1;
            if ( mCurrentIndex < HOTKEYS.length )
                next = HOTKEYS[mCurrentIndex++];
            return next;
        }
    }

}