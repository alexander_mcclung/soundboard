package sound
{
    import flash.events.Event;
    import flash.events.EventDispatcher;
    import flash.events.IOErrorEvent;
    import flash.events.NetMonitorEvent;
    import flash.events.SampleDataEvent;
    import flash.utils.ByteArray;

    import flash.media.Sound;
    import flash.media.SoundChannel;
    import flash.media.SoundTransform;

    import flash.net.URLRequest;

    import events.SoundEffectEvent;

    /**
     * Sound Effect Class that plays sound effects
     * @author amcclung
     */
    [Event(name="soundComplete", type="Event")]
    public class SoundEffect extends EventDispatcher
    {
        private const MAX_VOLUME:Number = 2;
        private const MIN_VOLUME:Number = 0;

        private const BLOCK_SIZE:int = 3072;

        private var mSource:Sound;
        private var mSound:Sound;
        private var mSoundTransform:SoundTransform;
        private var mSoundChannel:SoundChannel;
        private var mSoundLoaded:Boolean = false;
        private var mSoundPlaying:Boolean = false;

        private var mSoundLocation:String;

        private var mIsMaster:Boolean = false;

        private var mBytes:ByteArray;
        private var mRate:Number = 1.0;
        private var mScaledBlockSize:Number = mRate * BLOCK_SIZE;
        private var mPosition:Number = 0;

        /// Create a sound effect
        public function SoundEffect(location:String, volume:Number, master:Boolean)
        {
            mSoundLocation = location;
            mSoundChannel = new SoundChannel();

            mSoundTransform = new SoundTransform();
            mSoundTransform.volume = volume;

            mSource = new Sound( new URLRequest(location) );
            mSource.addEventListener(Event.COMPLETE, _OnSoundLoaded);
            mSource.addEventListener(IOErrorEvent.IO_ERROR, _OnSoundError);

            mSound = new Sound();

            mIsMaster = master;

            mBytes = new ByteArray();
        }

        /// Shut the sound down
        public function Shutdown():void
        {
            mSoundChannel.removeEventListener(Event.SOUND_COMPLETE, _OnSoundComplete);
            mSoundChannel.stop();
        }

        /// Returns whether or not this is supposed to be a master sound effect
        public function get Master():Boolean { return mIsMaster; }

        /// Get the location of the file that this sound was created from
        public function get Location():String { return mSoundLocation; }

        /// Get the sound's current playback volume
        public function get Volume():Number { return mSoundTransform.volume; }

        /// Set the playback volume of the sound effec
        public function set Volume(newVolume:Number):void
        {
            mSoundTransform.volume = newVolume;

            // Keep the volume in range
            if ( mSoundTransform.volume < MIN_VOLUME )
                mSoundTransform.volume = MIN_VOLUME;
            else if ( mSoundTransform.volume > MAX_VOLUME )
                mSoundTransform.volume = MAX_VOLUME;

            // Apply it to the sound channel for real-time updates
            mSoundChannel.soundTransform = mSoundTransform;

            // Tell the world I've changed
            dispatchEvent( new SoundEffectEvent(SoundEffectEvent.EVENT_SOUND_VOLUME_CHANGED) );
        }

        /// Set the playback rate of the sound effect. Affects speed and pitch.
        public function set Rate(newRate:Number):void
        {
            mRate = Math.max(0.1, newRate);
            mScaledBlockSize = mRate * BLOCK_SIZE;

            dispatchEvent( new SoundEffectEvent(SoundEffectEvent.EVENT_SOUND_RATE_CHANGED) );
        }

        public function get Rate():Number
        {
            return mRate;
        }

        /// Play the sound.
        public function Play():void
        {
            mSoundChannel.stop();

            mPosition = 0.0;

            mSound = new Sound();

            mSound.addEventListener( SampleDataEvent.SAMPLE_DATA, _OnSampleSound );

            mSoundChannel = mSound.play(0, 0, mSoundTransform);
            mSoundChannel.addEventListener(Event.SOUND_COMPLETE, _OnSoundComplete);

            mSoundPlaying = true;

            dispatchEvent( new SoundEffectEvent(SoundEffectEvent.EVENT_SOUND_STARTED) );
        }

        /// Returns whether or not the sound is currently playing.
        public function get Playing():Boolean
        {
            return mSoundPlaying;
        }

        /// Stop the sound. Will also dispatch the sound complete event handler.
        public function Stop():void
        {
            if ( mSoundPlaying )
            {
                mSoundChannel.removeEventListener(Event.SOUND_COMPLETE, _OnSoundComplete);
                mSoundChannel.stop();
                mSoundPlaying = false;
                dispatchEvent( new Event(Event.SOUND_COMPLETE) );
            }
        }

        /// Sound completion event handler
        private function _OnSoundComplete(e:Event):void
        {
            mSoundPlaying = false;
            mSoundChannel.removeEventListener(Event.SOUND_COMPLETE, _OnSoundComplete);

            dispatchEvent( new Event(Event.SOUND_COMPLETE) );
        }

        /// Sound loaded event handler
        private function _OnSoundLoaded(e:Event):void
        {
            mSoundLoaded = true;
            mSource.removeEventListener(Event.SOUND_COMPLETE, _OnSoundComplete);
        }

        private function _OnSoundError(e:IOErrorEvent):void
        {
            trace("Unable to load sound " + mSoundLocation + ": " + e.text );
        }

        private function _OnSampleSound(e:SampleDataEvent):void
        {
            mBytes.position = 0;

            var position:int = mPosition;
            var alpha:Number = mPosition - position;
            var positionTargetNum:Number = alpha;
            var positionTargetInt:int = -1;

            // Compute # of samples needed to process the block (+2 for interpolation)
            var numSamples:int = Math.ceil( mScaledBlockSize ) + 2;

            // Extract samples
            var read:int = mSource.extract( mBytes, numSamples, position );
            var n:int = read == numSamples ? BLOCK_SIZE : (read / mRate);

            if ( 0 == n )
            {
                mSound.removeEventListener(SampleDataEvent.SAMPLE_DATA, _OnSampleSound);
                return;
            }

            var l0:Number;
            var l1:Number;
            var r0:Number;
            var r1:Number;

            var i:int = 0;
            for (; i < n; i++)
            {
                // Avoid reading equal samples, if rate < 1
                if ( int(positionTargetNum) != positionTargetInt )
                {
                    positionTargetInt = positionTargetNum;

                    // set target read position
                    mBytes.position = positionTargetInt << 3;

                    // read two stereo samples for linear interpolation
                    l0 = mBytes.readFloat();
                    r0 = mBytes.readFloat();

                    l1 = mBytes.readFloat();
                    r1 = mBytes.readFloat();
                }

                // Write interpolated amplitudes into the stream
                e.data.writeFloat( l0 + alpha * (l1 - l0) );
                e.data.writeFloat( r0 + alpha * (r1 - r0) );

                // Increase target position
                positionTargetNum += mRate;

                // increase fraction and clamp between 0 and 1
                alpha += mRate;
                while ( alpha >= 1.0)
                    --alpha;
            }

            // Fill rest of stream with zeroes
            while ( i < BLOCK_SIZE )
            {
                e.data.writeFloat( 0.0 );
                e.data.writeFloat( 0.0 );
                ++i;
            }

            // Increase sound position
            mPosition += mScaledBlockSize;

            if ( mPosition > (mSource.bytesTotal * 8) )
            {
                mSoundChannel.stop();
            }
        }
    }
}