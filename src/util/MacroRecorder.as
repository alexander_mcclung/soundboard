package util
{
    import flash.events.Event;
    import flash.utils.getTimer;
    import macro.*;
    import sound.SoundEffect;
    import events.SoundEffectEvent;

    /**
     * Reords macros directly from the something.
     * @author Alex McClung
     */
    public class MacroRecorder
    {
        private var mRecording:Boolean = false;
        private var mLastSoundEventTime:int;
        private var mMacro:Macro;
        private var mSoundBank:Object;

        /// Create the macro recorder
        /// @param soundBank [Object] A bank of SoundEffects, indexed by file path.
        public function MacroRecorder(soundBank:Object)
        {
            mSoundBank = soundBank;
        }

        /// Begin recording the macro.
        public function BeginRecord():void
        {
            if ( !mRecording )
            {
                var effect:SoundEffect;
                for (var name:String in mSoundBank)
                {
                    effect = mSoundBank[name];
                    effect.addEventListener( SoundEffectEvent.EVENT_SOUND_STARTED,       _OnSoundStarted );
                    effect.addEventListener( SoundEffectEvent.EVENT_SOUND_RATE_CHANGED,  _OnSoundRateChanged );
                }

                mMacro = new Macro();
                mRecording = true;
                mLastSoundEventTime = -1;
            }
        }

        /// End recording.
        public function EndRecord():Macro
        {
            var temp:Macro = null;

            if ( mRecording )
            {
                var effect:SoundEffect;
                for (var name:String in mSoundBank)
                {
                    effect = mSoundBank[name];
                    effect.removeEventListener( SoundEffectEvent.EVENT_SOUND_STARTED,       _OnSoundStarted );
                    effect.removeEventListener( SoundEffectEvent.EVENT_SOUND_RATE_CHANGED,  _OnSoundRateChanged );
                }

                mRecording = false;
                temp = mMacro;
                mMacro = null;
            }

            return temp;
        }

        /// Returns whether or not the macro is currently recording
        public function get Recording():Boolean
        {
            return mRecording;
        }

        //{ region Sound Event Listeners

        /// Handles a sound started event. Adds a new sound step to the macro.
        private function _OnSoundStarted(e:SoundEffectEvent):void
        {
            var snd:SoundEffect = e.currentTarget as SoundEffect;
            _AddStep( new SoundStep(snd, false, snd.Rate ) );
        }

        /// Handles a sound rate changed event. Adds a new pitch change step to the macro.
        private function _OnSoundRateChanged(e:SoundEffectEvent):void
        {
            var snd:SoundEffect = e.currentTarget as SoundEffect;
            _AddStep( new SoundPitchStep( snd, snd.Rate ) );
        }
        //} endregion

        /// Add a step to the macro with an appropriate delay step, which represents the
        /// amount of time since the last call to _AddStep was made, before ite.
        private function _AddStep(step:MacroStep):void
        {
            var time:int = getTimer();

            // get the time since the last recorded sound event and add a wait step
            if ( -1 != mLastSoundEventTime )
            {
                mMacro.AddStep( new WaitStep(time - mLastSoundEventTime) );
            }

            mMacro.AddStep( step );

            mLastSoundEventTime = time;
        }

    }

}