package util
{
    /**
     * ...
     * @author amcclung
     */
    public class Range
    {
        private var mMin:Number = 0;
        private var mMax:Number = 1;
        private var mSpan:Number = 1;

        public function Range(min:Number, max:Number)
        {
            SetRange( min, max );
        }

        public function SetRange(min:Number, max:Number):void
        {
            mMin = min;
            mMax = max;
            mSpan = mMax - mMin;
        }

        public function get Min():Number { return mMin; }
        public function set Min(min:Number):void
        {
            mMin = min;
            mSpan = mMax - mMin;
        }

        public function get Max():Number { return mMax; }
        public function set Max(max:Number):void
        {
            mMax = max;
            mSpan = mMax - mMin;
        }

        public function get Span():Number { return mSpan; }

        public function ValueOf(percent:Number, clamp:Boolean=false):Number
        {
            var value:Number = (percent * mSpan) + mMin;
            if ( clamp )
            {
                if ( value > mMax )
                    value = mMax;
                if ( value < mMin )
                    value = mMin;
            }
            return value;
        }

        public function PercentOf(value:Number):Number
        {
            if ( 0 == mSpan )
                return 0;

            return (value - mMin) / mSpan;
        }
    }
}