package util
{
    import macro.*;

    /**
     * Builds a macro from an XML node
     * @author Alex McClung
     */
    public class MacroBuilder
    {
        public static function BuildMacro(soundBank:Object, node:XML):Macro
        {
            var m:Macro = new Macro();

            var children:XMLList = node.children();
            var step:MacroStep;
            var name:String;

            for each(var child:XML in children)
            {
                step = null;
                name = child.name();

                switch(name)
                {
                    case "wait":
                    {
                        step = _BuildWaitStep( child );
                        break;
                    }
                    case "sound":
                    {
                        step = _BuildSoundStep( soundBank, child );
                        break;
                    }
                }

                if ( null != step )
                {
                    m.AddStep( step );
                }
            }
            return m;
        }

        private static function _BuildWaitStep(node:XML):MacroStep
        {
            var step:MacroStep = null;

            if ( node.hasOwnProperty("@duration") )
            {
                step = new WaitStep( int(node.@duration) );
            }

            return step;
        }

        private static function _BuildSoundStep(soundBank:Object, node:XML):MacroStep
        {
            var step:MacroStep = null;

            if ( node.hasOwnProperty("@location") )
            {
                var location:String = node.@location;
                var wait:Boolean = node.hasOwnProperty("@wait") ? Boolean(node.@wait) : false;
                var rate:Number = node.hasOwnProperty("@rate") ? Number(node.@rate) : 1.0;

                step = new SoundStep( soundBank[location], wait, rate );
            }

            return step;
        }
    }

}