package controls
{
    import flash.display.BlendMode;
    import flash.display.Sprite;
    import flash.events.Event;
    import flash.events.MouseEvent;
    import flash.geom.Point;
    import util.Range;

    /**
     * ...
     * @author amcclung
     */
    public class Slider extends UiControl
    {
        protected var mRange:Range;
        protected var mCurrentValue:Number;
        protected var mDragging:Boolean = false;

        protected var mBackground:Sprite;
        protected var mForeground:Sprite;

        public function Slider(minValue:Number, maxValue:Number, currentValue:Number)
        {
            mRange = new Range(minValue, maxValue);
            mCurrentValue = currentValue;

            mBackground = new Sprite();
            addChild( mBackground );

            mForeground = new Sprite();
            mForeground.blendMode = BlendMode.OVERLAY;
            addChild( mForeground );

            addEventListener( MouseEvent.MOUSE_DOWN, _StartDrag    );
            addEventListener( MouseEvent.CLICK,      _OnMouseClick );
        }

        public function Shutdown():void
        {
            removeEventListener( MouseEvent.MOUSE_DOWN, _StartDrag    );
            removeEventListener( MouseEvent.CLICK,      _OnMouseClick );

            stage.removeEventListener( MouseEvent.MOUSE_MOVE, _OnMouseMove );
            stage.removeEventListener( MouseEvent.MOUSE_UP,   _StopDrag );
        }

        public function set CurrentValue(value:Number):void
        {
            if ( value > mRange.Max )
                value = mRange.Max;
            else if ( value < mRange.Min )
                value = mRange.Min;

            mCurrentValue = value;
            dispatchEvent( new Event(Event.CHANGE) );

            _Draw();
        }
        public function get CurrentValue():Number { return mCurrentValue; }

        //{ region Mouse Events
        private function _OnMouseClick(e:MouseEvent):void
        {
            e.stopImmediatePropagation();
        }

        private function _StartDrag(e:MouseEvent):void
        {
            stage.addEventListener( MouseEvent.MOUSE_MOVE, _OnMouseMove );
            stage.addEventListener( MouseEvent.MOUSE_UP,   _StopDrag );

            _OnMouseMove(e);

            e.stopImmediatePropagation();
        }

        private function _StopDrag(e:MouseEvent):void
        {
            stage.removeEventListener( MouseEvent.MOUSE_MOVE, _OnMouseMove );
            stage.removeEventListener( MouseEvent.MOUSE_UP,   _StopDrag );
        }

        private function _OnMouseMove(e:MouseEvent):void
        {
            var local:Point = globalToLocal( new Point(e.stageX, e.stageY) );

            //CurrentValue = mRange.ValueOf( (mHeight - local.y) / mHeight, true );
            CurrentValue = mRange.ValueOf( local.x / mWidth, true );
        }
        //} endregion

        override protected function _Draw():void
        {
            super._Draw();

            var width:Number = mRange.PercentOf( mCurrentValue ) * mWidth;

            // Draw foreground
            mForeground.graphics.clear();
            mForeground.graphics.beginFill( mColor, 0.5 );
            //graphics.drawRect( 0, mHeight - height, mWidth, height );
            mForeground.graphics.drawRect( 0, 0, width, mHeight );
            mForeground.graphics.endFill();

            // Draw background
            mBackground.graphics.clear();
            mBackground.graphics.beginFill( 0x000000, 0.5 );
            mBackground.graphics.drawRect( width, 0, mWidth-width, mHeight );
            mBackground.graphics.endFill();
        }
    }

}