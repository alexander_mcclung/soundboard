package controls
{
    import events.SoundEffectEvent;
    import sound.SoundEffect;

    /**
     * ...
     * @author amcclung
     */
    public class PitchSlider extends Slider
    {
        private var mSound:SoundEffect;

        public function PitchSlider(sound:SoundEffect, min:Number, max:Number, current:Number)
        {
            super(min, max, current);

            mSound = sound;
            mSound.addEventListener( SoundEffectEvent.EVENT_SOUND_RATE_CHANGED, _OnRateChanged );
        }

        override public function Shutdown():void
        {
            mSound.removeEventListener( SoundEffectEvent.EVENT_SOUND_RATE_CHANGED, _OnRateChanged );
            mSound = undefined;

            super.Shutdown();
        }

        /// The sound's pitch has changed. Updates my current value and redraws.
        /// Does not dispatch any events.
        private function _OnRateChanged(e:SoundEffectEvent):void
        {
            var snd:SoundEffect = e.target as SoundEffect;

            mCurrentValue = snd.Rate;
            _Draw();
        }

        /// My slider value changed. Update the sound's volume and redraw
        override public function set CurrentValue(value:Number):void
        {
            super.CurrentValue = value;

            // Stop listening for events while we update the sound's playback rate. Otherwise we wind up
            // in an infinite loop and blow out the callstack
            mSound.removeEventListener( SoundEffectEvent.EVENT_SOUND_RATE_CHANGED, _OnRateChanged );
            mSound.Rate = mCurrentValue;
            mSound.addEventListener( SoundEffectEvent.EVENT_SOUND_RATE_CHANGED, _OnRateChanged );
        }

    }

}