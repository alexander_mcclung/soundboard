package controls
{
    import flash.display.Sprite;

    /**
     * ...
     * @author amcclung
     */
    public class UiControl extends Sprite
    {
        protected var mWidth:Number  = 0;
        protected var mHeight:Number = 0;
        protected var mColor:Number  = 0xFFFFFF;

        public function UiControl()
        {
            super();
        }

        /// Set the control's color
        public function SetColor(color:Number):void
        {
            mColor = color;
            _Draw();
        }

        /// Set the size of the control
        /// @param w [Number] New value for the control's width
        /// @param h [Number] New value for the control's height
        public function SetSize(w:Number, h:Number):void
        {
            mWidth  = w;
            mHeight = h;
            _Draw();
        }

        public function get Width():Number { return mWidth; }
        public function set Width(w:Number):void
        {
            mWidth = w;
            _Draw();
        }

        public function get Height():Number { return mHeight; }
        public function set Height(h:Number):void
        {
            mHeight = h;
            _Draw();
        }

        protected function _Draw():void
        {
            // subclasses should do this
        }

    }

}