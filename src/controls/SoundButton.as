package controls
{
    import events.SoundEffectEvent;
    import flash.events.Event;
    import flash.events.KeyboardEvent;
    import flash.events.MouseEvent;
    import flash.media.Sound;
    import flash.text.AntiAliasType;
    import sound.SoundEffect;

    import flash.display.CapsStyle;
    import flash.display.JointStyle;

    import flash.text.TextField;
    import flash.text.TextFormat;
    import flash.text.TextFormatAlign;

    /**
     * A button that plays a sound when clicked, or when its hotkey has been pressed
     * @author amcclung
     */
    public class SoundButton extends Button
    {
        private var mSound:SoundEffect;
        private var mFadeStep:Number;
        private var mOriginalVolume:Number;
        private var mRandomRate:Boolean = false; /// Whether or not to randomize the sound's playback rate
        private var mPitchSlider:Slider;
        private var mVolumeSlider:Slider;

        /// Make a button that plays a sound
        public function SoundButton(label:String, sound:SoundEffect)
        {
            super(label);

            mSound = sound;
            mSound.addEventListener( Event.SOUND_COMPLETE, _OnSoundComplete );
            mSound.addEventListener( SoundEffectEvent.EVENT_SOUND_STARTED, _OnSoundStarted );

            var sliderThickness:int = 6;
            mPitchSlider = new PitchSlider(mSound, 0.5, 1.5, 1.0);
            mPitchSlider.SetSize( sliderThickness, sliderThickness );
            addChild( mPitchSlider );

            mVolumeSlider = new VolumeSlider(mSound, 0.0, 2.0, mSound.Volume);
            mVolumeSlider.SetSize( sliderThickness, sliderThickness );
            addChild( mVolumeSlider );
        }

        /// Shut the button down
        override public function Shutdown():void
        {
            mSound.Shutdown();
            removeChild(mInfo);

            super.Shutdown();
        }

        /// Stop the sound currently being played.
        public function Stop():void
        {
            if ( mSound.Playing )
            {
                mSound.Stop();
            }
        }

        /// Mouse click event handler
        override protected function _OnClick(e:Event):void
        {
            super._OnClick(e);

            if ( mRandomRate )
            {
                mPitchSlider.CurrentValue = 0.5 + (int(Math.random() * 10) / 10);
                mSound.Rate = mPitchSlider.CurrentValue;
            }

            mSound.Play();

            _Draw();
        }

        /// Handle a keyboard key press
        override public function HandleKeyPress(e:KeyboardEvent):Boolean
        {
            if ( e.shiftKey && !mRandomRate )
            {
                mRandomRate = true;
            }

            return super.HandleKeyPress(e);
        }

        override public function HandleKeyRelease(e:KeyboardEvent):Boolean
        {
            if ( !e.shiftKey && mRandomRate )
            {
                mRandomRate = false;
            }

            return super.HandleKeyRelease(e);
        }

        /// Mouse wheel event handler
        override protected function _OnMouseWheel(e:MouseEvent):void
        {
            super._OnMouseWheel(e);

            const STEP:Number = 0.1;

            if (e.delta > 0 )
            {
                mSound.Volume += STEP;
                _Draw();
            }
            else if ( e.delta < 0 )
            {
                mSound.Volume -= STEP;
                _Draw();
            }
        }

        /// Draw the button
        override protected function _Draw():void
        {
            super._Draw();

            mInfo.text = "";
            if ( -1 != mHotKeyCode )
            {
                mInfo.appendText( mHotKeyChar );
            }

            var paddingX:int = 8;
            var paddingY:int = 3;

            mPitchSlider.Width = mWidth - (2 * paddingX);
            mPitchSlider.x = paddingX;
            mPitchSlider.y = mHeight - mPitchSlider.height - paddingY - 5;

            mVolumeSlider.x = mPitchSlider.x;
            mVolumeSlider.Width = mPitchSlider.Width;
            mVolumeSlider.y = mPitchSlider.y - mVolumeSlider.Height - paddingY;
        }

        /// Draw the background
        override protected function _DrawBackground():void
        {
            var alpha:Number = UNFOCUS_ALPHA;

            // Draw big yellow square if the sound is playing
            if ( mSound.Playing )
            {
                alpha = ACTIVE_ALPHA;

                mBackground.graphics.lineStyle( 2, 0xFFFFFF, 1.0, true, null, CapsStyle.SQUARE, JointStyle.MITER );
            }
            else if ( mMouseOver )
            {
                alpha = FOCUS_ALPHA;
            }

            mBackground.graphics.beginFill(mColor, alpha);
            mBackground.graphics.drawRect( 0, 0, mWidth, mHeight );
        }

        /// Sound complete event handler; re-dispaches the event to other listeners.
        private function _OnSoundComplete(e:Event):void
        {
            _Draw();
        }

        /// My sound effect was started. I should probably draw a yellow border around myself or something.
        private function _OnSoundStarted(e:Event):void
        {
            _Draw();
        }
    }

}