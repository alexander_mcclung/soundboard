package controls
{
    import events.SoundEffectEvent;
    import flash.display.DisplayObject;
    import sound.SoundEffect;

    /**
     * Custom slider class that controls the volume of a sound effect
     * @author amcclung
     */
    public class VolumeSlider extends Slider
    {
        private var mSound:SoundEffect;

        public function VolumeSlider(sound:SoundEffect, min:Number, max:Number, current:Number)
        {
            super(min, max, current);

            mSound = sound;
            mSound.addEventListener( SoundEffectEvent.EVENT_SOUND_VOLUME_CHANGED, _OnVolumeChanged );
        }

        override public function Shutdown():void
        {
            mSound.removeEventListener( SoundEffectEvent.EVENT_SOUND_VOLUME_CHANGED, _OnVolumeChanged );
            mSound = undefined;

            super.Shutdown();
        }

        /// The sound's volume has changed. Updates my current value and redraws.
        /// Does not dispatch any events.
        private function _OnVolumeChanged(e:SoundEffectEvent):void
        {
            var snd:SoundEffect = e.target as SoundEffect;

            mCurrentValue = snd.Volume;
            _Draw();
        }

        /// My slider value changed. Update the sound's volume and redraw
        override public function set CurrentValue(value:Number):void
        {
            super.CurrentValue = value;

            // Stop listening for events while we update the sound's volume. Otherwise we wind up
            // in an infinite loop and blow out the callstack
            mSound.removeEventListener( SoundEffectEvent.EVENT_SOUND_VOLUME_CHANGED, _OnVolumeChanged );
            mSound.Volume = mCurrentValue;
            mSound.addEventListener( SoundEffectEvent.EVENT_SOUND_VOLUME_CHANGED, _OnVolumeChanged );
        }
    }

}