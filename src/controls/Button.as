package controls
{
    import flash.display.BlendMode;
    import flash.display.CapsStyle;
    import flash.display.JointStyle;
    import flash.display.Sprite;
    import flash.ui.Mouse;

    import flash.events.Event;
    import flash.events.KeyboardEvent;
    import flash.events.MouseEvent;

    import flash.text.AntiAliasType;
    import flash.text.Font;
    import flash.text.TextField;
    import flash.text.TextFormat;
    import flash.text.TextFormatAlign;

    import flash.ui.Keyboard;

    /**
     * A base button class for all of your button-related needs.
     * @author Alex McClung
     */
    public class Button extends UiControl
    {
        // Visual Things
        protected const UNFOCUS_ALPHA:Number  = 0.5;
        protected const FOCUS_ALPHA:Number    = 0.8;
        protected const ACTIVE_ALPHA:Number   = 1.0;

        protected var mBackground:Sprite;
        protected var mLabel:TextField;
        protected var mInfo:TextField;
        protected var mLabelString:String;

        protected var mHotKeyCode:Number = -1;
        protected var mHotKeyChar:String = "?";
        private var mLabelText:String = "";

        protected var mMouseOver:Boolean = false;

        /// Constructor
        public function Button(label:String)
        {
            super();

            mLabelText = label;

            cacheAsBitmap = true;

            mLabelString = label;

            mBackground = new Sprite();
            addChild( mBackground );

            // Init texts
            var fmt:TextFormat = new TextFormat("Open Sans Condensed Light", 18, 0xFFFFFF);

            mLabel = new TextField();
            mLabel.text = mLabelString;
            mLabel.selectable = false;
            mLabel.wordWrap = true;
            mLabel.antiAliasType = AntiAliasType.ADVANCED;
            mLabel.setTextFormat( fmt );
            mLabel.defaultTextFormat = fmt;
            mLabel.embedFonts = true;
            mLabel.x = 5;
            mLabel.y = 2;

            addChild( mLabel );

            var format:TextFormat = new TextFormat("Open Sans Condensed Light", 12, 0xFFFFFF);
            format.align = TextFormatAlign.RIGHT;

            mInfo = new TextField();
            mInfo.selectable = false;
            mInfo.wordWrap = false;
            mInfo.antiAliasType = AntiAliasType.ADVANCED;
            mInfo.setTextFormat( format );
            mInfo.defaultTextFormat = format;
            mInfo.embedFonts = true;
            addChild( mInfo );

            addEventListener(MouseEvent.CLICK,        _OnClick       );
            addEventListener(MouseEvent.MOUSE_OVER,   _OnMouseOver   );
            addEventListener(MouseEvent.MOUSE_OUT,    _OnMouseOut    );
            addEventListener(MouseEvent.MOUSE_WHEEL,  _OnMouseWheel  );
        }

        /// Set the keyboard key used to play this button's sound
        public function SetHotKey(keyCode:Number):void
        {
            mHotKeyCode = keyCode;

            switch( keyCode )
            {
                case Keyboard.EQUAL:        mHotKeyChar = "=";  break;
                case Keyboard.MINUS:        mHotKeyChar = "-";  break;
                case Keyboard.BACKQUOTE:    mHotKeyChar = "`";  break;
                case Keyboard.LEFTBRACKET:  mHotKeyChar = "[";  break;
                case Keyboard.RIGHTBRACKET: mHotKeyChar = "]";  break;
                case Keyboard.BACKSLASH:    mHotKeyChar = "\\"; break;
                case Keyboard.SLASH:        mHotKeyChar = "/";  break;
                case Keyboard.SEMICOLON:    mHotKeyChar = ";";  break;
                case Keyboard.QUOTE:        mHotKeyChar = "'";  break;
                case Keyboard.COMMA:        mHotKeyChar = ",";  break;
                case Keyboard.PERIOD:       mHotKeyChar = ".";  break;
                default:
                    mHotKeyChar = String.fromCharCode(keyCode);
                    break;
            }

            _Draw();
        }

        /// Shutdowner
        public function Shutdown():void
        {
            removeEventListener(MouseEvent.CLICK,       _OnClick);
            removeEventListener(MouseEvent.MOUSE_OVER,  _OnMouseOver);
            removeEventListener(MouseEvent.MOUSE_OUT,   _OnMouseOut);
            removeEventListener(MouseEvent.MOUSE_WHEEL, _OnMouseWheel);

            removeChild( mLabel );
            removeChild( mBackground );
        }

        //{ region Keyboard Events
        /// Handle a keyboard key press event
        public function HandleKeyPress(e:KeyboardEvent):Boolean
        {
            if ( e.keyCode == mHotKeyCode )
            {
                _OnClick(null);
                return true;
            }
            return false;
        }

        /// Handle a keyboard key release event
        public function HandleKeyRelease(e:KeyboardEvent):Boolean
        {
            return false;
        }
        //} endregion

        //{ region Mouse Events
        /// mouse wheel event handler, adjusts the volume of this button's sound effect only
        protected function _OnMouseWheel(e:MouseEvent):void
        {
            // Subclasses should implement this
        }

        /// MouseOut event handler, unfocuses the button
        protected function _OnMouseOut(e:Event):void
        {
            mMouseOver = false;
            _Draw();
        }

        /// MouseOver event handler, focuses the button
        protected function _OnMouseOver(e:Event):void
        {
            mMouseOver = true;
            _Draw();
        }

        /// Mouse click event handler; plays the sound.
        protected function _OnClick(e:Event):void
        {
            // Subclasses should do their own thing
        }
        //} endregion

        /// Draws the button
        override protected function _Draw():void
        {
            super._Draw();

            mLabel.text = mLabelText;

            mBackground.graphics.clear();

            _DrawBackground();

            mLabel.width = mWidth - (2 * mLabel.x);
            mLabel.height = mHeight - 20;

            mInfo.x = mLabel.x;
            mInfo.y = mLabel.y + mLabel.textHeight - mInfo.textHeight - 2;
            mInfo.width = mLabel.width;
            mInfo.height = 20;
        }

        protected function _DrawBackground():void
        {
            var alpha:Number = mMouseOver ? FOCUS_ALPHA : UNFOCUS_ALPHA;

            mBackground.graphics.beginFill(mColor, alpha);
            mBackground.graphics.drawRect( 0, 0, mWidth, mHeight );
        }
    }
}