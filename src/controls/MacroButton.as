package controls 
{
    import flash.events.Event;
    import flash.text.TextField;
    import flash.text.TextFormat;
    import flash.text.TextFormatAlign;
    import macro.Macro;
    /**
     * ...
     * @author Alex McClung
     */
    public class MacroButton extends Button
    {
        private var mMacro:Macro;
        
        public function MacroButton(label:String, macro:Macro) 
        {
            super(label);
            
            mMacro = macro;
        }
        
        public function SetMacro( m:Macro ):void
        {
            mMacro = m;
            _Draw();
        }
        
        override public function Shutdown():void 
        {
            mMacro = null;
            super.Shutdown();
        }
        
        override protected function _OnClick(e:Event):void 
        {
            super._OnClick(e);
            
            if ( null != mMacro )
            {
                mMacro.Execute();
            }
        }
        
        override protected function _Draw():void
        {
            super._Draw();

            mInfo.text = "";
            if ( -1 != mHotKeyCode )
            {
                mInfo.appendText( mHotKeyChar );
            }
        }
    }

}