package controls 
{
    import flash.events.Event;
    import flash.display.CapsStyle;
    import flash.display.JointStyle;
    import flash.events.KeyboardEvent;
    
    import macro.Macro;
    import util.MacroRecorder;
    
    /**
     * ...
     * @author Alex McClung
     */
    public class MacroRecordButton extends Button
    {
        private var mRecorder:MacroRecorder;
        private var mMacro:Macro;
        private var mRecordOnPress:Boolean = false;
        
        public function MacroRecordButton(label:String, soundBank:Object) 
        {
            super( label );
            
            mRecorder = new MacroRecorder( soundBank );
        }
        
        override public function HandleKeyPress(e:KeyboardEvent):Boolean 
        {
            if ( e.shiftKey && !mRecordOnPress )
            {
                mRecordOnPress = true;
                _Draw();
            }
            
            return super.HandleKeyPress(e);
        }
        
        override public function HandleKeyRelease(e:KeyboardEvent):Boolean 
        {
            if ( !e.shiftKey && mRecordOnPress )
            {
                mRecordOnPress = false;
                _Draw();
            }
            
            return super.HandleKeyRelease(e);
        }
        
        override protected function _OnClick(e:Event):void 
        {
            super._OnClick(e);
            
            if ( mRecorder.Recording )
            {
                mMacro = mRecorder.EndRecord();
            }
            else if ( mRecordOnPress )
            {
                mRecorder.BeginRecord();
            }
            else if( null != mMacro )
            {
                mMacro.Execute();
            }
            
            _Draw();
        }
        
        override protected function _Draw():void 
        {
            super._Draw();
            
            if ( mRecordOnPress )
            {
                mLabel.appendText( " (Record)");
            }
            
            if ( mRecorder.Recording )
            {
                mInfo.text = "RECORDING";
            }
            else
            {
                mInfo.text = "";
            }
        }
        
        override protected function _DrawBackground():void 
        {
            if ( mRecorder.Recording )
            {
                mBackground.graphics.lineStyle( 2, 0xFFFFFF, ACTIVE_ALPHA, true, null, CapsStyle.SQUARE, JointStyle.MITER );
            }
            super._DrawBackground();
        }
    }

}