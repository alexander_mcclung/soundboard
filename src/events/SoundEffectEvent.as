package events
{
    import flash.events.Event;

    /**
     * Event dispatched by the SoundEffect class.
     */
    public class SoundEffectEvent extends Event
    {
        /// Dispatched whenever a sound is started
        public static const EVENT_SOUND_STARTED:String = "soundStarted";

        public static const EVENT_SOUND_RATE_CHANGED:String = "rateChanged";
        public static const EVENT_SOUND_VOLUME_CHANGED:String = "volumeChanged";

        public function SoundEffectEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
        {
            super(type, bubbles, cancelable);
        }
    }

}