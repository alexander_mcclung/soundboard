package
{
    import flash.display.Loader;
    import flash.display.Sprite;
    import flash.display.StageAlign;
    import flash.display.StageScaleMode;
    import flash.filters.DropShadowFilter;
    import util.MacroRecorder;

    import macro.*;

    import sound.SoundEffect;
    import struct.HotKeys;

    import util.MacroBuilder;

    import flash.events.Event;
    import flash.events.KeyboardEvent;
    import flash.events.MouseEvent;
    import flash.events.ProgressEvent;

    import flash.net.URLLoader;
    import flash.net.URLRequest;

    import flash.ui.Keyboard;
    import flash.xml.XMLNode;
    import flash.text.Font;

    import layout.Grid;
    import controls.*;

    /**
     * The Main class. Loads the config file and puts stuff on the stage.
     * @author Alex McClung
     */
    public class Main extends Sprite
    {
        /// Embeds
        [Embed(source = '../bin/fonts/OpenSans-CondLight.ttf', fontName = 'Open Sans Condensed Light',embedAsCFF='false',
        unicodeRange='U+0020-U+002F,U+0030-U+0039,U+003A-U+0040,U+0041-U+005A,U+005B-U+0060,U+0061-U+007A,U+007B-U+007E')]
        public static const OPEN_SANS_CONDENSED_LIGHT:Class;

        /// Stuff that loads XML and other stuff
        private var mLoader:URLLoader;
        private var mImageLoader:Loader;

        /// List of sound buttons. Or the Stop button. Or empty buttons. Or Macro Buttons.
        /// Pretty much anything that's on the stage that's not the background
        /// is going into this array
        private var mButtons:Array;

        /// List of all Sound Effects in the sound board, indexed by sound file location
        private var mSoundBank:Object;

        private var mFadingSounds:Array;

        /// Layout Grids
        private var mSoundButtonGrid:Grid;
        private var mMacroButtonGrid:Grid;
        private var mPadding:int = 0;
        private var mMarginTop:int = 0;
        private var mMarginRight:int = 0;
        private var mMarginBottom:int = 0;
        private var mMarginLeft:int = 0;

        /// Background image
        private var mBackground:Sprite;
        private var mButtonMount:Sprite;

        /// HotKey supplier
        private var mHotKeys:HotKeys;

        public function Main():void
        {
            if (stage)
            {
                init();
            }
            else
            {
                addEventListener(Event.ADDED_TO_STAGE, init);
            }
        }

        // added to stage event handler
        private function init(e:Event = null):void
        {
            removeEventListener(Event.ADDED_TO_STAGE, init);

            stage.showDefaultContextMenu = false;
            stage.addEventListener("rightClick", _OnRightClick);

            Font.registerFont(OPEN_SANS_CONDENSED_LIGHT);

            var fonts:Array = Font.enumerateFonts();
            for each(var font:Font in fonts)
            {
                trace( font.fontName + ":" + font.fontType );
            }

            mButtons   = new Array();
            mSoundBank = new Object();
            mHotKeys   = new HotKeys();

            mBackground = new Sprite();
            addChild( mBackground );

            var bgFilters:Array = new Array();
            bgFilters.push( new DropShadowFilter(0.0, 0.0, 0x000000, 0.29, 5, 5 ) );

            mButtonMount = new Sprite();
            mButtonMount.filters = bgFilters;
            addChild( mButtonMount );

            stage.scaleMode = StageScaleMode.NO_SCALE;
            stage.align = StageAlign.TOP_LEFT;

            stage.addEventListener( Event.RESIZE, _OnStageResize );

            mImageLoader = new Loader();
            mImageLoader.contentLoaderInfo.addEventListener( Event.COMPLETE, _OnBackgroundLoded );

            mSoundButtonGrid = new Grid(0,0);
            mMacroButtonGrid = new Grid(0,0);

            _LoadConfig();
        }

        private function _OnRightClick(e:MouseEvent):void
        {

        }

        // Load the main configuration xml file. Will clear the stage of any elements while the load is in progress.
        private function _LoadConfig():void
        {
            stage.removeEventListener(KeyboardEvent.KEY_DOWN, _OnKeyDown);

            // Remove all buttons first
            var temp:Button;
            while ( mButtons.length > 0 )
            {
                temp = mButtons.pop();
                temp.Shutdown();
                mButtonMount.removeChild( temp );
            }
            mSoundButtonGrid.Clear();
            mMacroButtonGrid.Clear();

            // Clear the sound bank
            for (var name:String in mSoundBank)
            {
                mSoundBank[name] = undefined;
            }
            mSoundBank = new Object();

            // Reset hotkeys
            mHotKeys.Reset();

            // remove the background image, too, if it's even in the display tree
            try
            {
                removeChild( mImageLoader );
            }
            catch (er:Error)
            {
                // continue;
            }

            mLoader = new URLLoader( new URLRequest("SoundBoard.xml") );
            mLoader.addEventListener(Event.COMPLETE, _OnXmlLoaded);
        }

        // Background has finished loading
        private function _OnBackgroundLoded(e:Event):void
        {
            mBackground.addChild( mImageLoader );
            _OnStageResize(null);
        }

        // Config.xml has finished loading
        private function _OnXmlLoaded(e:Event):void
        {
            var doc:XML = new XML(e.target.data);

            // Load the background image
            mImageLoader.load( new URLRequest( doc.layout.background.text() ) );

            // Init layout variables
            mPadding      = int(doc.layout.padding.text());
            mMarginTop    = int(doc.layout.margintop.text());
            mMarginRight  = int(doc.layout.marginright.text());
            mMarginBottom = int(doc.layout.marginbottom.text());
            mMarginLeft   = int(doc.layout.marginleft.text());
            mSoundButtonGrid = new Grid( int(doc.layout.columns.text()), int(doc.layout.rows.text()) );
            mSoundButtonGrid.SetPadding( mPadding );
            mSoundButtonGrid.SetOrigin( mMarginLeft, mMarginTop );

            var btn:Button;
            var snd:SoundEffect;

            var sounds:XMLList = doc.sounds.sound;

            // Build the buttons and sounds
            var numSounds:Number = doc.sounds.sound.length();
            var key:int;

            for ( var i:int = 0; i < numSounds; i++)
            {
                // Use the builders to build a sound effect and a button. See what happens from there.
                snd = _BuildSound( sounds[i] );
                btn = _BuildSoundButton( sounds[i], snd );

                // If we got a sound-effect, we have to do some extra work
                if ( null != snd )
                {
                    // Non-master sounds get a hotkey
                    if ( !snd.Master )
                    {
                        key = mHotKeys.Next();
                        if( -1 != key )
                            btn.SetHotKey( key );
                    }
                    // Otherwise, we listen for when master sound buttons finish playing sounds
                    else
                    {
                        snd.addEventListener(Event.SOUND_COMPLETE, _MasterSoundComplete);
                    }

                    mSoundBank[ snd.Location ] = snd;
                }

                mButtonMount.addChild( btn );
                mButtons.push( btn );
                mSoundButtonGrid.AddButton( btn );
            }

            // Build the macros
            var macros:XMLList = doc.macros.macro;
            var numMacros:Number = macros.length();
            var macroColor:int = 0x666666;

            mMacroButtonGrid = new Grid( numMacros + 1, 1 );
            mMacroButtonGrid.SetPadding( mPadding );

            for (var j:int = 0; j < numMacros; j++)
            {
                btn = _BuildMacroButton( macros[j] );
                btn.SetColor( macroColor );

                mButtonMount.addChild( btn );
                mButtons.push( btn );
                mMacroButtonGrid.AddButton( btn );
            }

            // build the custom buttons
            btn = new MacroRecordButton( "Custom", mSoundBank );
            btn.SetColor( macroColor );
            mButtonMount.addChild( btn );
            mButtons.push( btn );
            mMacroButtonGrid.AddButton( btn );

            // "Resize" the stage once to get all the buttons drawn, then start listening for
            // keyboard events.
            _OnStageResize(null);
            stage.addEventListener(KeyboardEvent.KEY_DOWN, _OnKeyDown);
            stage.addEventListener(KeyboardEvent.KEY_UP, _OnKeyUp);
        }

        /// Special event handler for the master sound completion event
        private function _MasterSoundComplete(e:Event):void
        {
            trace("Master sound complete.");
            _StopAllSounds();
        }

        /// Stops all the sounds that are currently being played
        private function _StopAllSounds():void
        {
            mFadingSounds = new Array();

            var snd:SoundEffect;
            for (var name:String in mSoundBank)
            {
                snd = mSoundBank[name];
                if ( snd.Playing )
                    mFadingSounds.push( {sound:snd, originalVolume:snd.Volume, step:snd.Volume / (1500 / (1000/stage.frameRate)) } );
            }

            if ( mFadingSounds.length > 0 )
                addEventListener(Event.ENTER_FRAME, _FadeStep);
        }

        /// Callback from within the Stop() function's OnEnterFrame. Steps the sound volume down one more notch.
        /// Will clear the enterframe when the volume reaches 0.
        private function _FadeStep(e:Event):void
        {
            var temp:Object;
            for (var i:int = 0; i < mFadingSounds.length; i++)
            {
                temp = mFadingSounds[i];

                if ( temp.sound.Volume <= 0 )
                {
                    temp.sound.Volume = temp.originalVolume;
                    temp.sound.Stop();

                    mFadingSounds.splice(0, 1);
                    --i;
                }
                else
                {
                    temp.sound.Volume -= temp.step;
                }
            }

            // If we've faded out all the sounds, stop doing enter frame stuff
            if ( mFadingSounds.length == 0 )
            {
                removeEventListener(Event.ENTER_FRAME, _FadeStep);
            }
        }

        /// Keyboard button handler
        private function _OnKeyDown(e:KeyboardEvent):void
        {
            switch(e.keyCode)
            {
                case Keyboard.F12:
                {
                    _LoadConfig();
                    break;
                }
                case Keyboard.ESCAPE:
                {
                    _StopAllSounds();
                    break;
                }

                default:
                {
                    for (var i:int = 0; i < mButtons.length; i++)
                    {
                        if ( mButtons[i].HandleKeyPress(e) )
                            break;
                    }
                    break;
                }
            }
        }

        private function _OnKeyUp(e:KeyboardEvent):void
        {
            for (var i:int = 0; i < mButtons.length; i++)
            {
                if ( mButtons[i].HandleKeyRelease(e) )
                {
                    break;
                }
            }
        }

        /// Main stage/window resize event handler. Resizes all the buttons and the background.
        private function _OnStageResize(e:Event):void
        {
            const MACRO_ROW_HEIGHT:int = 48;

            var gridWidth:Number = stage.stageWidth - (mMarginLeft + mMarginRight);
            var gridHeight:Number = stage.stageHeight - (mMarginTop + mMarginBottom);

            mSoundButtonGrid.SetSize( gridWidth, gridHeight - MACRO_ROW_HEIGHT - mPadding );
            mSoundButtonGrid.Draw();

            mMacroButtonGrid.SetOrigin( mMarginLeft, mMarginTop + gridHeight - MACRO_ROW_HEIGHT );
            mMacroButtonGrid.SetSize( gridWidth, MACRO_ROW_HEIGHT );
            mMacroButtonGrid.Draw();

            mBackground.width = stage.stageWidth;
            mBackground.height = stage.stageHeight;
        }

        //{ region Builder Helper Functions
        // These functions build instance of...things from XML nodes.

        /// Build a button from an XML node.
        private function _BuildSoundButton(node:XML, sound:SoundEffect):Button
        {
            var button:Button = null;

            var label:String = node.@label;

            if ( null != sound )
                button = new SoundButton( label, sound );
            else
                button = new Button(label);

            if ( null != button )
            {
                if ( node.hasOwnProperty("@color") )
                    button.SetColor( Number(node.@color) );
            }

            return button;
        }

        /// Build a sound from an XML node
        private function _BuildSound(node:XML):SoundEffect
        {
            var sound:SoundEffect = null;

            if ( node.hasOwnProperty("@location") && node.@location != "" )
            {
                var location:String = node.@location;

                var volume:Number = 1.0;
                if ( node.hasOwnProperty('@volume') )
                    volume = Number( node.@volume );

                var master:Boolean = false;
                if ( node.hasOwnProperty('@master') )
                    master = Boolean(node.master);

                sound = new SoundEffect(location, volume, master);
            }

            return sound;
        }

        /// Build a button from a macro xml node
        private function _BuildMacroButton(node:XML):Button
        {
            // Build the macro first
            var m:Macro = MacroBuilder.BuildMacro( mSoundBank, node );


            // Then build the button
            var label:String = node.@label;
            var btn:MacroButton = new MacroButton( node.@label, m );

            return btn;
        }
        //} endregion
    }
}